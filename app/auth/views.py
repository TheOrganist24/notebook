from flask import render_template, redirect, request, url_for, flash
from flask_login import login_user, logout_user, login_required, current_user
from . import auth
from .. import db
from ..models import Users
from .forms import LoginForm, RegistrationForm, ChangePasswordForm, ChangeEmailForm
import datetime

@auth.route('/login', methods=['GET', 'POST'])
def login():
  form = LoginForm()
  if form.validate_on_submit():
    user = Users.query.filter_by(email=form.email.data).first()
    if user is not None and user.verify_password(form.password.data):
      login_user(user, form.remember_me.data)
      next = request.args.get('next')
      if next is None or not next.startswith('/'):
        next = url_for('main.index')
      return redirect(next)
    flash('Invalid username or password.')
  return render_template('auth/login.html', form=form)

@auth.route('/logout')
@login_required
def logout():
  logout_user()
  flash('You have been logged out.')
  return redirect(url_for('main.index'))


@auth.route('/register', methods=['GET', 'POST'])
def register():
  form = RegistrationForm()
  if form.validate_on_submit():
    user = Users(email=form.email.data, username=form.username.data, password=form.password.data, name=form.name.data, key=form.key.data)
    db.session.add(user)
    db.session.commit()
    return redirect(url_for('main.index'))
  return render_template('auth/register.html', form=form)

@auth.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
  form = ChangePasswordForm()
  if form.validate_on_submit():
    if current_user.verify_password(form.old_password.data):
      current_user.password = form.password.data
      db.session.add(current_user)
      db.session.commit()
      flash('Your password has been updated.')
      return redirect(url_for('main.index'))
    else:
      flash('Invalid password.')
  return render_template("auth/change_password.html", form=form)


@auth.route('/change_email', methods=['GET', 'POST'])
@login_required
def change_email_request():
  form = ChangeEmailForm()
  if form.validate_on_submit():
    if current_user.verify_password(form.password.data):
      new_email = form.email.data
      return redirect(url_for('main.index'))
    else:
      flash('Invalid email or password.')
  return render_template("auth/change_email.html", form=form)
