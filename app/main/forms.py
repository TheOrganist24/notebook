from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, BooleanField, SelectField, SubmitField, IntegerField, FileField, PasswordField, DateField
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo
from wtforms import ValidationError
from datetime import date, time, timedelta
from time import strptime
import datetime

class CreateBook(FlaskForm):
  new_book = StringField('New Book')
  submit = SubmitField('Create')

class WriteNotes(FlaskForm):
  note_input = TextAreaField('Enter a note')
  submit = SubmitField('Submit')
