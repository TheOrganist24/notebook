from flask import render_template, redirect, url_for, abort, flash
from flask_login import login_required, current_user
from . import main
from .. import db
from time import strptime
import datetime
from sqlalchemy import extract, desc
from dateutil.relativedelta import relativedelta
from ..models import Users, Records
import hashlib as hasher
import os
import gnupg
from markdown2 import Markdown
from .forms import WriteNotes, CreateBook

# Define what a notes block is
class Block:
  def __init__(self, index, timestamp, data, previous_hash):
    self.index = index
    self.timestamp = timestamp
    self.data = data
    self.previous_hash = previous_hash
    self.hash = self.hash_block()
  
  def hash_block(self):
    sha = hasher.sha256()
    sha.update(str(self.index) + str(self.timestamp) + str(self.data) + str(self.previous_hash))
    return sha.hexdigest()

class getBooks:
  def __init__(self):
    notebooks = []
    notebooks_db_objects = Records.query.filter_by(owner = current_user.name).group_by(Records.book).all()
    for notebook_db_object in notebooks_db_objects:
      notebooks.append(notebook_db_object.book)
    self.books = notebooks

# Standard routes
@main.route('/', methods=['GET', 'POST'])
@login_required
def index():

  # Check if genesis block exists
  first_block = Records.query.filter_by(id = 1).first()
  if first_block is None:
    now = datetime.datetime.now()
    genesis_hash = Block(1, now, 'Genesis Block', "0").hash

    genesis_block = Records(hash = genesis_hash, date = now, note = 'Genesis Block')
    db.session.add(genesis_block)
    db.session.commit()

    flash('Blockchain initiated: Genesis hash ' + genesis_hash + ' created at ' + str(now))

  # Check whether private/public key exists
  if not os.path.exists('/home/' + Users.query.filter_by(name = current_user.name).first().username + '/.gnupg'):
    flash('Please create some gpg keys')

  form = CreateBook()
  if form.validate_on_submit():
    return redirect(url_for('main.notebook', book=form.new_book.data))

  return render_template('index.html', form=form, notebooks=getBooks().books)

@main.route('/about')
def about():
  return render_template('about.html', notebooks=getBooks().books)

@main.route('/health')
def health():
  return 'running'

@main.route('/notebook/<book>/', methods=['GET', 'POST'])
def notebook(book):
  gpg = gnupg.GPG(gnupghome='/home/' + Users.query.filter_by(name = current_user.name).first().username + '/.gnupg')

  note_db_objects = Records.query.filter_by(book=book).all()
  
  notes = ''
  for note_data in note_db_objects:

    decrypted_data = gpg.decrypt(note_data.note)

    markdowner = Markdown()
    html_data = markdowner.convert(str(decrypted_data))

    notes = notes + "<p align=right><b>" + str(note_data.date) + "</b></p>" + html_data

  presentation_notebook = 'Testing'

  form = WriteNotes()
  if form.validate_on_submit():
    data = form.note_input.data
    key = Users.query.filter_by(name = current_user.name).first().key
    encrypted_ascii_data = gpg.encrypt(data, key, sign=True)

    previous_hash_object = Records.query.order_by(Records.id.desc()).first()
    id = previous_hash_object.id + 1
    now = datetime.datetime.now()
    old_hash = previous_hash_object.hash

    new_hash = Block(id, now, encrypted_ascii_data, old_hash).hash

    new_block = Records(hash = new_hash, date = now, book = book, owner = current_user.name, note = str(encrypted_ascii_data))
    db.session.add(new_block)
    db.session.commit()
    print('Hash: ' + new_hash + ' has been recorded')

    return redirect(url_for('main.notebook', book = book))

  return render_template('notebook.html', notebooks = getBooks().books, presentation_notebook = notes, form = form)

## Run in virtualenvironment
#activate_this_file = "venv/bin/activate_this.py"
#execfile(activate_this_file, dict(__file__=activate_this_file))
#
#from flask import Flask
#from flask import request
#import json
#import requests
#import hashlib as hasher
#import datetime as date
#import os
#from optparse import OptionParser
#from os.path import expanduser
#import git
#import subprocess
#import webbrowser
#import time
#

#
#  # Calculate hash and add to record
#  now=date.datetime.now()
#  new_hash=Block(id, now, file_data, old_hash).hash
#
#  records_file=open(home + "/.bcnotes/data/records", "a+")
#  records_file.write(str(id) + "," + str(now) + "," + new_hash + "\n")
#  records_file.close()
#
#  print("Recorded hash: " + new_hash)
#
#  # Commit to git repo
#  repo_dir = os.path.join(home + "/.bcnotes/data")
#  file_name = os.path.join(repo_dir, str(id) + '.asc')
#  records_file_git = os.path.join(repo_dir, "records")
#
#  r = git.Repo.init(repo_dir)
#  r.index.add([file_name, records_file_git])
#  r.index.commit("Commit index: " + str(id))
#
#  print("Commited index: " + str(id))
