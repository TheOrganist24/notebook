from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask_login import UserMixin, AnonymousUserMixin
from flask import current_app, request
from sqlalchemy.orm import relationship
from . import db, login_manager
from alembic import op

class Users(UserMixin, db.Model):
  __tablename__ = 'users'
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(256))
  username = db.Column(db.String(256))
  email = db.Column(db.String(64), unique=True, index=True)
  password_hash = db.Column(db.String(128))
  key = db.Column(db.String(64))

  @property
  def password(self):
    raise AttributeError('password is not a readable attribute')

  @password.setter
  def password(self, password):
    self.password_hash = generate_password_hash(password)

  def verify_password(self, password):
    return check_password_hash(self.password_hash, password)

@login_manager.user_loader
def load_user(user_id):
  return Users.query.get(int(user_id))

class Records(db.Model):
  __tablename__ = 'records'
  id = db.Column(db.Integer, primary_key=True)
  hash = db.Column(db.String(256))
  date = db.Column(db.Text)
  book = db.Column(db.String(256))
  owner = db.Column(db.Integer, db.ForeignKey('users.id'))
  note = db.Column(db.Text)
