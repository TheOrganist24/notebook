#!/usr/bin/env python

# Run in virtualenvironment
activate_this_file = "venv/bin/activate_this.py"
execfile(activate_this_file, dict(__file__=activate_this_file))

from flask import Flask
from flask import request
import json
import requests
import hashlib as hasher
import datetime as date
import os
from optparse import OptionParser
from os.path import expanduser
import git
import gnupg
import subprocess
from markdown2 import Markdown
import webbrowser
import time

basedir = os.path.abspath(os.path.dirname(__file__))
node = Flask(__name__)
usage = "usage: %prog [options]"
parser = OptionParser(usage=usage)
home = expanduser("~")

# Define what a notes block is
class Block:
  def __init__(self, index, timestamp, data, previous_hash):
    self.index = index
    self.timestamp = timestamp
    self.data = data
    self.previous_hash = previous_hash
    self.hash = self.hash_block()
  
  def hash_block(self):
    sha = hasher.sha256()
    sha.update(str(self.index) + str(self.timestamp) + str(self.data) + str(self.previous_hash))
    return sha.hexdigest()


def init(option, opt_str, value, parser):

  # Check if data/ repo exists and is initialised as a git repo (initialise if not)
  if not os.path.exists(home + "/.bcnotes"):
    os.makedirs(home + "/.bcnotes")
  if not os.path.exists(home + "/.bcnotes/data"):
    os.makedirs(home + "/.bcnotes/data")

  if not os.path.exists(home +"/.bcnotes/data/initiated"):
    repo_dir = os.path.join(home + "/.bcnotes/data")
    file_name = os.path.join(repo_dir, 'initiated')

    r = git.Repo.init(repo_dir)
    open(file_name, 'wb').close()
    r.index.add([file_name])
    r.index.commit("Initial commit")

  print('Data repo in place\n')

  # Check whether private/public key exists and is set up (set one up otherwise)
  if not os.path.exists(home + "/.gnupg"):
    gnupg_home = home + "/.gnupg/"
    gpg = gnupg.GPG(gnupghome=gnupg_home)
    input_data = gpg.gen_key_input(key_type="RSA", key_length=1024, name_email=raw_input("Type your email address for the encryption key: "))
    key = gpg.gen_key(input_data)
    print("Generated key: " + str(key))

  print('Keys in place\n')

  # Creates and stores genesis block
  if not os.path.exists(home + "/.bcnotes/data/records"):
    now=date.datetime.now()
    gen_hash=Block(0, now, 'First Block', "0").hash
    print('Genesis hash ' + gen_hash + ' created at ' + str(now))

    records_file=open(home + "/.bcnotes/data/records", "w+")
    records_file.write("0," + str(now) + "," + gen_hash + "\n")
    records_file.close()

    r.index.add([records_file.name])
    r.index.commit("Added genesis block")

  print('Genesis hash recorded\n')

def submit(option, opt_str, value, parser):

  # Sign and encrypt document and store (under index stamp)
  gpg=gnupg.GPG()
  key_id=gpg.list_keys()[0]['keyid']
  file_to_encrypt=home + "/bcnote.md"

  records_file = open(home + "/.bcnotes/data/records", "r")
  last_line = records_file.readlines()[-1]
  records_file.close()

  last_line_list=last_line.split(',')
  id=int(last_line_list[0])+1
  old_hash=last_line_list[2].rstrip()

  infile=open(file_to_encrypt, "rb")
  encrypted_data=gpg.encrypt_file(infile, key_id, sign=True, output=home + "/.bcnotes/data/" + str(id) + '.asc')
  infile.close()
  open(file_to_encrypt, 'w').close()
  with open(home + "/.bcnotes/data/" + str(id) + '.asc', 'r') as data_file:
    file_data=data_file.read()

  # Calculate hash and add to record
  now=date.datetime.now()
  new_hash=Block(id, now, file_data, old_hash).hash

  records_file=open(home + "/.bcnotes/data/records", "a+")
  records_file.write(str(id) + "," + str(now) + "," + new_hash + "\n")
  records_file.close()

  print("Recorded hash: " + new_hash)

  # Commit to git repo
  repo_dir = os.path.join(home + "/.bcnotes/data")
  file_name = os.path.join(repo_dir, str(id) + '.asc')
  records_file_git = os.path.join(repo_dir, "records")

  r = git.Repo.init(repo_dir)
  r.index.add([file_name, records_file_git])
  r.index.commit("Commit index: " + str(id))

  print("Commited index: " + str(id))


def view(option, opt_str, value, parser):

  print 'This section is currently under development'
  # Decrypt files and concatenate
  gpg=gnupg.GPG()
  key_id=gpg.list_keys()[0]['keyid']

  records_file = open(home + "/.bcnotes/data/records", "r")

  last_line = records_file.readlines()[-1]

  last_line_list=last_line.split(',')
  id=int(last_line_list[0])
  now=str(last_line_list[1])
  records_file.close()

  records_file = open(home + "/.bcnotes/data/records", "r")
  lines = records_file.readlines()
  records_file.close()

  markdown_data=''
  for file_id in range(1, id+1):
    data_date_list=lines[file_id].split(',')
    data_date=str(data_date_list[1])
    file_name = home + "/.bcnotes/data/" + str(file_id) + ".asc"
    infile = open(file_name, "rb")
    decrypted_data=gpg.decrypt_file(infile)
    markdown_data = markdown_data + "<p align=right><b>" + data_date + "</b></p>\n" + str(decrypted_data) + "\n"

  # Convert to html
  markdowner = Markdown()
  html_data = markdowner.convert(markdown_data)
  
  html_file = open(home + "/.bcnotes/notes.html", "w+")
  html_file.write(html_data)
  html_file.close()

  # Make viewable in web browser
  webbrowser.open("file://" + home + "/.bcnotes/notes.html", new=2)
  time.sleep(5)
  os.remove(home + "/.bcnotes/notes.html")

def upload(option, opt_str, value, parser):
  print 'This section is currently under development'
  # Run git pull and git push on data repo

def prove(option, opt_str, value, parser):
  print 'This section is currently under development'
  # Test validity of blockchain

parser.add_option("-i", "--init", action="callback", callback=init, help="Initialises the data repo")
parser.add_option("-s", "--submit", action="callback", callback=submit, help="Submits a file title `bcnotes.md`")
parser.add_option("-v", "--view", action="callback", callback=view, help="Views notebook")
parser.add_option("-u", "--upload", action="callback", callback=upload, help="Synchronises with git repo")
parser.add_option("-p", "--prove", action="callback", callback=prove, help="Test validity of blockchain")

(options, args) = parser.parse_args()
