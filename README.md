# Notebook
> An encrypted, blockchained notebook

## Abstract
Well written handwritten notebooks are advantageous over digital versions because they are far harder to fake reliably, and can provide a useful and truthful history - complete with crossings out and mistakes. This project intends to make use of encryption and blockchain to make a reliable and secure digital notebook.

## Design
### Backend
Encryption will be acheived using the gnupg encryption system (based on pgp). The blockchain will be stored in local sqlite databases (or a text file) which can then be distibuted amongst all nodes. The different nodes can be desktops in a network, or desktops spread between multiple people using the same notebook.

The notebook needs to be viewable easily, ideally through a gui.

## Getting Started
**Placement:** this app should be cloned into `/opt/applications/webapps/`. A number of files live in the `install/` directory, these should be placed as follows:
1. `cd /opt/applications/webapps/notebook`
1. `sudo mv install/notebook.service /etc/systemd/system/notebook.service`
1. `sudo mv install{notebook.sh,settings.conf} .`

**Configuration:** this app needs access to an email account and a database. These can be specified in the `settings.conf`:
* `FLASK_*` can be left as is

**Startup:** to start the app, use systemd to start the service; this will kick off the startup script which manages database migrations. On first run this could take quite a while as there is a lot of data in the lookup tables to populate from NRE.
1. `sudo systemctl daemon-reload`
1. `sudo systemctl enable notebook.service`
1. `sudo systemctl start notebook.service`

If you want to watch the startup do `sudo journalctl -u notebook.service -f`

**Recommendations:** you should run the webapp behind some sort of frontend, nginx and apache are both suitable.

## Versioning

## Attributions
* My [wife](https://gitlab.com/tudor-rose) and [father-in-law](https://gitlab.com/michaelallen1966), for their patience and help
