#!/bin/bash

DIR=/opt/applications/webapps/notebook

cd $DIR

logger --tag notebook --priority local3.info "Changed into working directory"

if [ -a venv ]; then
  logger --tag notebook --priority local3.info "Activating virtual environment"
  source $DIR/venv/bin/activate && logger --tag notebook --priority local3.info "...success" || logger --tag notebook --priority local3.err "...failed"; 
else
  logger --tag notebook --priority local3.info "Building virtual envrionment"
  /usr/bin/virtualenv $DIR/venv && logger --tag notebook --priority local3.info "...success" || logger --tag notebook --priority local3.err "...failed";

  logger --tag notebook --priority local3.info "Activating virtual envrionment"
  source $DIR/venv/bin/activate && logger --tag notebook --priority local3.info "...success" || logger --tag notebook --priority local3.err "...failed";

fi

logger --tag notebook --priority local3.info "Installing requirements"
$DIR/venv/bin/pip install -r $DIR/requirements.txt && logger --tag notebook --priority local3.info "...success" || logger --tag notebook --priority local3.err "...failed";

logger --tag notebook --priority local3.info "Sourcing settings"
source $DIR/settings.conf && logger --tag notebook --priority local3.info "...success" || logger --tag notebook --priority local3.err "...failed";

logger --tag notebook --priority local3.info "Migrating database"
$DIR/venv/bin/flask db upgrade && logger --tag notebook --priority local3.info "...success" || logger --tag notebook --priority local3.err "...failed";

logger --tag notebook --priority local3.info "Starting web server"
GUNICORN="$DIR/venv/bin/gunicorn"
$GUNICORN --bind 0.0.0.0:5001 --reload wsgi && logger --tag notebook --priority local3.info "Code exited" || logger --tag notebook --priority local3.err "...failed"

